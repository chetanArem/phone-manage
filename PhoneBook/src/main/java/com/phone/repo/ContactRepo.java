package com.phone.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phone.model.Contact;

public interface ContactRepo extends JpaRepository<Contact, Long> 
{

}
