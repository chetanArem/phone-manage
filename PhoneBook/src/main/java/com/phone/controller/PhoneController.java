package com.phone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.phone.model.Contact;
import com.phone.repo.ContactRepo;

@RestController
public class PhoneController
{
	@Autowired
	private ContactRepo contactRepo ;
	
	@GetMapping(value="/")
	public String getPage()
	{
		return "Welcome";
	}

	@GetMapping(value="/contacts")
	public List<Contact> getContacts()
	{
		return contactRepo.findAll();
	}
	
	@PostMapping(value="/save")
	public String saveContact(@RequestBody Contact contact)
	{
		contactRepo.save(contact);
		return "Saved..";
	}
	
	
	
}
